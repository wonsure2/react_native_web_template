#!/bin/bash

set -e
. ~/.nvm/nvm.sh

# nvm install node@6.2.2
echo "********** nvm install begin **********"
nvm install v6.2.2
echo "********** node install success **********"

{{#gecko}}unlink node_modules{{/gecko}}
# npm install node_modules
echo "********** npm install node_modules begin **********"
npm_auto install --registry "http://npm.byted.org/" --verbose
echo "********** npm install node_modules success! **********"

# run build_online
echo "********** build_android_rn begin **********"
npm run build_online
echo "********** build_android_rn success! **********"

mkdir output
# template to output/template
mv dist/template output/

{{#gecko}}
# TODO 上传资源包到 gecko
echo '开始上传资源包...'

ACCESS_KEY='c62aa74b670fe26bd5f653a0c8c41e51,77027d45a48c3dc839b896f3d334b1a5' # 前内测，后线上
# FILE_NAME=$(ls ./output/)
CHANNEL='rn_inapp' # ${FILE_NAME%.*} 使用ZIP包的文件名作为渠道名，实际项目中应按自己的需求配置
MESSAGE=$(git log -1 HEAD --pretty=format:%s)
COMMIT_ID=$(git log -1 HEAD --pretty=format:%H)
NOTIFICATION_RECEIVER='wangshuo.frontend' # 钉钉通知列表（邮箱前缀），多个通知对象以逗号分隔

response=$(curl \
    -F "package=@./output/$CHANNEL.zip" \
    -F "access_key=$ACCESS_KEY" \
    -F "description=$MESSAGE" \
    -F "commit_id=$COMMIT_ID" \
    -F "channel=$CHANNEL" \
    -F "scm_version=$BUILD_VERSION" \
    -F "notification_receiver=$NOTIFICATION_RECEIVER" \
    https://gecko.bytedance.net/gecko/server/upload_candidate_package)

status=$(echo $response | grep -Eo '"status":\s*[0-9]+' | grep -Eo '[0-9]+')

if [[ $status == 0 ]]
then
    echo '资源包上传成功'
else
    echo '资源包上传失败，请重新编译'
    echo $response
    exit 1
fi
{{/gecko}}

echo "********** fe_build done **********"